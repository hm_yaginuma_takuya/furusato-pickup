class OauthController < ApplicationController

  def callback
    auth = NeAPI::Auth.new(redirect_url: '/callback')
    auth_response = auth.ne_auth(params["uid"], params["state"], ENV['NE_API_CLIENT_ID'], ENV['NE_API_CLIENT_SECRET'])

    client = NeAPI::Master.new(
      access_token: auth_response['access_token'],
      refresh_token: auth_response['refresh_token']
    )

    company_info = client.login_company_info['data'][0]
    user_info    = client.login_user_info

    company = Company.find_by(main_function_id_hash: company_info['company_id'])
    company = company_filter(company, company_info)

    session[:company_id] = company.id
    session[:user_id]    = user_filter(company, user_info).id

    redirect_to root_path
  end

  def company_filter(company, company_information)
    company.present? ? company : create_company(company_information)
  end

  # @param [Hash] company_information
  # @return [Company]
  def create_company(company_information)
    company = Company.new(
      main_function_id_hash: company_information['company_id'],
      ne_company_id_hash: company_information['company_ne_id'],
      name: company_information['company_name'],
      name_kana: company_information['company_kana']
    )

    unless company.save
      raise '企業情報の登録に失敗しました'
    end

    company
  end

  # userが作成済みでない時だけ新規作成してuserオブジェクトを返す
  # @param [Company] company
  # @param [Hash] user_information
  # @return [User]
  def user_filter(company, user_information)
    user = User.find_by(uid: user_information['data'][0]['uid'])

    if user.present?
      update_token(user, user_information['access_token'], user_information['refresh_token'])
      user
    else
      create_user(company.id, user_information)
    end
  end

  # トークンを更新するための共通処理
  # @param [User] user APIを実行するユーザーのUserオブジェクト
  # @param [String] access_token アクセストークン
  # @param [String] refresh_token リフレッシュトークン
  def update_token(user, access_token, refresh_token)
    user.access_token  = access_token
    user.refresh_token = refresh_token

    unless user.save
      raise 'アクセストークンとリフレッシュトークンの更新に失敗しました'
    end
  end

  # @param [Int] company_id
  # @param [Hash] user_information
  # @return [User]
  def create_user(company_id, user_information)
    user = User.new(
      company_id: company_id,
      uid: user_information['data'][0]['uid'],
      pic_id: user_information['data'][0]['pic_id'],
      pic_ne_id: user_information['data'][0]['pic_ne_id'],
      pic_name: user_information['data'][0]['pic_name'],
      pic_kana: user_information['data'][0]['pic_kana'],
      pic_mail_address: user_information['data'][0]['pic_mail_address'],
      user_role_id: user_information['user_role_id'],
      access_token: user_information['access_token'],
      refresh_token: user_information['refresh_token']
    )
    unless user.save
      raise 'ユーザー情報の登録に失敗しました'
    end

    user
  end
end
