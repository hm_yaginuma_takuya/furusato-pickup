Rails.application.routes.draw do
  root to: 'pickups#index'

  resources :posts

  get 'pickups', to: 'pickups#index'

  # callback
  get 'callback', to: 'oauth#callback'
end
