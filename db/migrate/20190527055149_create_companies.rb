class CreateCompanies < ActiveRecord::Migration[6.0]
  def change
    create_table :companies do |t|
      t.string :ne_company_id_hash
      t.string :main_function_id_hash
      t.string :name
      t.string :name_kana

      t.timestamps
    end
  end
end
