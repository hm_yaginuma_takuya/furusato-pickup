class CreateUsers < ActiveRecord::Migration[6.0]
  def change
    create_table :users do |t|
      t.integer :company_id
      t.string :uid
      t.integer :pic_id
      t.string :pic_ne_id
      t.string :pic_name
      t.string :pic_kana
      t.string :pic_mail_address
      t.integer :user_role_id
      t.string :access_token
      t.string :refresh_token

      t.timestamps
    end
  end
end
